package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import connection.ConnectionFactory;
import model.bean.Passageiro;

public class PassageiroDAO {
	
	public void create(Passageiro p) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try {
			stmt = con.prepareStatement("INSERT INTO Passageiro (nome, cpf, rg, email, endereco, genero, telefone) VALUES (?,?,?,?,?,?,?)");
			stmt.setString(1, p.getNome());
			stmt.setLong(2, p.getCpf());
			stmt.setLong(3, p.getRg());
			stmt.setString(4, p.getEmail());
			stmt.setString(5, p.getEndereco());
			stmt.setString(6, p.getGenero());
			stmt.setString(7, p.getTelefone());
			
			stmt.executeUpdate();
			JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
		}catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao salvar" + e);
		} finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}
	
	public List<Passageiro> read(){
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Passageiro> passageiros = new ArrayList<>();
		
		try {
			stmt = con.prepareStatement("SELECT * FROM passageiro");
			rs = stmt.executeQuery();
			while(rs.next()) {
				Passageiro p = new Passageiro();
				
				p.setId_passageiro(rs.getInt("ID_PASSAGEIRO"));
				p.setNome(rs.getString("NOME"));
				p.setEmail(rs.getString("EMAIL"));
				p.setEndereco(rs.getString("ENDERECO"));
				p.setTelefone(rs.getString("TELEFONE"));
				p.setGenero(rs.getString("GENERO"));
				p.setCpf(rs.getLong("CPF"));
				p.setRg(rs.getLong("RG"));
				passageiros.add(p);
				
			}
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao exibir as informa��es do BD" + e);
			e.printStackTrace();
		}finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
		return passageiros;
	}
	
	public void delete(Passageiro p) {
				
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
				
		try {
			stmt = con.prepareStatement("DELETE FROM passageiro WHERE ID_PASSAGEIRO = ?");
			stmt.setInt(1, p.getId_passageiro());
			stmt.executeUpdate();
				
			JOptionPane.showMessageDialog(null, "Passageiro exclu�do com sucesso!");
		}catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao excluir: "+ e);
		}finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}
	
	public Passageiro read(int id) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Passageiro p = new Passageiro();
		
		try {
			stmt = con.prepareStatement("SELECT * FROM passageiro WHERE ID_PASSAGEIRO = ? LIMIT 1;");
			stmt.setInt(1, id);
			rs = stmt.executeQuery();
			if(rs != null && rs.next()) {
				p.setId_passageiro(rs.getInt("ID_PASSAGEIRO"));
				p.setNome(rs.getString("NOME"));
				p.setEmail(rs.getString("EMAIL"));
				p.setEndereco(rs.getString("ENDERECO"));
				p.setTelefone(rs.getString("TELEFONE"));
				p.setGenero(rs.getString("GENERO"));
				p.setCpf(rs.getLong("CPF"));
				p.setRg(rs.getLong("RG"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
			return p;
		}
			
			
	public void update(Passageiro p) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
				
		try {
			stmt = con.prepareStatement("UPDATE passageiro    "
									  + "	SET NOME = ?,     "
									  + "       EMAIL = ?,    "
									  + "       ENDERECO = ?, "
									  + "       TELEFONE = ?, "
									  + "       GENERO = ?,   "
									  + "       CPF = ?,      "
									  + "       RG = ?        "
									  + " WHERE ID_PASSAGEIRO = ?;  ");
			stmt.setString(1, p.getNome());
			stmt.setString(2, p.getEmail());
			stmt.setString(3, p.getEndereco());
			stmt.setString(4, p.getTelefone());
			stmt.setString(5, p.getGenero());
			stmt.setLong(6, p.getCpf());
			stmt.setLong(7, p.getRg());
			stmt.setInt(8, p.getId_passageiro());
			stmt.executeUpdate();
					
			JOptionPane.showMessageDialog(null, "Alterado com sucesso!");
		}catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao alterar" + e);
		} finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}
	
}
