package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class JFCadPassageiro extends JFrame {

	private JPanel contentPane;
	private JTextField cpoNome;
	private JTextField cpoEmail;
	private JTextField cpoTelefone;
	private JTextField cpoEndereco;
	private JTextField cpoCpf;
	private JTextField cpoRg;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFCadPassageiro frame = new JFCadPassageiro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFCadPassageiro() {
		setTitle("Cadastro de Passageiros");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 333, 344);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCadastrarPassageiro = new JLabel("Cadastrar Passageiro:");
		lblCadastrarPassageiro.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblCadastrarPassageiro.setBounds(10, 10, 138, 21);
		contentPane.add(lblCadastrarPassageiro);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNome.setBounds(10, 41, 45, 13);
		contentPane.add(lblNome);
		
		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblEmail.setBounds(10, 66, 45, 13);
		contentPane.add(lblEmail);
		
		JLabel lblTelefone = new JLabel("Telefone:");
		lblTelefone.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblTelefone.setBounds(10, 93, 69, 13);
		contentPane.add(lblTelefone);
		
		JLabel lblEndereco = new JLabel("Endereco:");
		lblEndereco.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblEndereco.setBounds(10, 122, 69, 13);
		contentPane.add(lblEndereco);
		
		JLabel lblCpf = new JLabel("CPF:");
		lblCpf.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblCpf.setBounds(10, 151, 45, 13);
		contentPane.add(lblCpf);
		
		JLabel lblRg = new JLabel("RG:");
		lblRg.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblRg.setBounds(10, 176, 45, 13);
		contentPane.add(lblRg);
		
		JLabel lblGenero = new JLabel("Genero:");
		lblGenero.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblGenero.setBounds(10, 204, 55, 13);
		contentPane.add(lblGenero);
		
		cpoNome = new JTextField();
		cpoNome.setToolTipText("Nome");
		lblNome.setLabelFor(cpoNome);
		cpoNome.setBounds(75, 39, 96, 19);
		contentPane.add(cpoNome);
		cpoNome.setColumns(10);
		
		cpoEmail = new JTextField();
		cpoEmail.setToolTipText("Email\r\n");
		lblEmail.setLabelFor(cpoEmail);
		cpoEmail.setColumns(10);
		cpoEmail.setBounds(75, 64, 199, 19);
		contentPane.add(cpoEmail);
		
		cpoTelefone = new JTextField();
		cpoTelefone.setToolTipText("Telefone\r\n");
		lblTelefone.setLabelFor(cpoTelefone);
		cpoTelefone.setColumns(10);
		cpoTelefone.setBounds(75, 91, 96, 19);
		contentPane.add(cpoTelefone);
		
		cpoEndereco = new JTextField();
		cpoEndereco.setToolTipText("Endere\u00E7o");
		lblEndereco.setLabelFor(cpoEndereco);
		cpoEndereco.setColumns(10);
		cpoEndereco.setBounds(75, 120, 199, 19);
		contentPane.add(cpoEndereco);
		
		cpoCpf = new JTextField();
		cpoCpf.setToolTipText("CPF(11): 00000000000");
		lblCpf.setLabelFor(cpoCpf);
		cpoCpf.setColumns(10);
		cpoCpf.setBounds(75, 145, 96, 19);
		contentPane.add(cpoCpf);
		
		cpoRg = new JTextField();
		cpoRg.setToolTipText("RG(10) : 0000000000");
		lblRg.setLabelFor(cpoRg);
		cpoRg.setColumns(10);
		cpoRg.setBounds(75, 174, 96, 19);
		contentPane.add(cpoRg);
		
		JComboBox cpoGenero = new JComboBox();
		lblGenero.setLabelFor(cpoGenero);
		cpoGenero.setModel(new DefaultComboBoxModel(new String[] {"Masculino", "Feminino", "Outro"}));
		cpoGenero.setBounds(75, 201, 96, 21);
		contentPane.add(cpoGenero);
		
		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if ( (cpoNome.getText() == "")                     ||
					 (cpoCpf.getText().matches("-?\\d+") == false) ||
					 (cpoCpf.getText().length() != 11)             ||
					 (cpoEmail.getText() == "")                    ||
					 (cpoRg.getText().matches("-?\\d+") == false)  ||
					 (cpoRg.getText().length() != 10)              ||
					 (cpoEndereco.getText() == "")                 ||
					 (cpoTelefone.getText() == "") ) {
					
					JOptionPane.showMessageDialog(null, "For�o encontrados erros nos Dados inseridos! Favor Verificar.");
	
				} else {
				
					Passageiro p = new Passageiro();
					PassageiroDAO dao = new PassageiroDAO();
					
					p.setNome(cpoNome.getText());
					p.setCpf(Long.parseLong(cpoCpf.getText()));
					p.setEmail(cpoEmail.getText());
					p.setRg(Long.parseLong(cpoRg.getText()));
					p.setGenero(cpoGenero.getSelectedItem().toString());
					p.setEndereco(cpoEndereco.getText());
					p.setTelefone(cpoTelefone.getText());
					
					dao.create(p);
					dispose();
				}
				
			}
		});
		btnSalvar.setBounds(10, 245, 85, 21);
		contentPane.add(btnSalvar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setBounds(189, 245, 85, 21);
		contentPane.add(btnCancelar);
		
		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cpoNome.setText("");
				cpoCpf.setText("");
				cpoEmail.setText("");
				cpoRg.setText("");
				cpoCpf.setText("");
				cpoGenero.setSelectedIndex(0);
				cpoEndereco.setText("");
				cpoTelefone.setText("");
			}
		});
		btnLimpar.setBounds(101, 245, 85, 21);
		contentPane.add(btnLimpar);
		
		
	}
}
