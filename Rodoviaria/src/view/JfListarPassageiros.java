package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class JfListarPassageiros extends JFrame {

	private JPanel contentPane;
	private JTable JTPassageiros;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JfListarPassageiros frame = new JfListarPassageiros();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JfListarPassageiros() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent e) {
				readJTable();
			}
		});
		setTitle("Listar Passageiros");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 563, 518);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblListarPassageiros = new JLabel("Listar Passageiros");
		lblListarPassageiros.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblListarPassageiros.setBounds(10, 10, 161, 19);
		contentPane.add(lblListarPassageiros);
		
		JScrollPane scrollPanelTabelaPassageiros = new JScrollPane();
		scrollPanelTabelaPassageiros.setBounds(20, 39, 506, 382);
		contentPane.add(scrollPanelTabelaPassageiros);
		
		JTPassageiros = new JTable();
		JTPassageiros.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null},
			},
			new String[] {
				"ID do Passageiro", "Nome", "Genero", "RG", "CPF", "Telefone", "Email", "Endere\u00E7o"
			}
		));
		JTPassageiros.getColumnModel().getColumn(0).setPreferredWidth(115);
		scrollPanelTabelaPassageiros.setViewportView(JTPassageiros);
		
		JButton btnNewButton = new JButton("Cadastrar Passageiro");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFCadPassageiro cp = new JFCadPassageiro();
				cp.setVisible(true);	
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnNewButton.setBounds(10, 431, 190, 21);
		contentPane.add(btnNewButton);
		
		JButton btnAlterarFilme = new JButton("Alterar Passageiro");
		btnAlterarFilme.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//verificar a linha selecionada
				if(JTPassageiros.getSelectedRow()!= -1) {
					JFAtualizarPassageiro ap = new JFAtualizarPassageiro((int)JTPassageiros.getValueAt(JTPassageiros.getSelectedRow(),0));
					ap.setVisible(true);	
				} else {
					JOptionPane.showMessageDialog(null, "Selecione um passageiro!");
				}
				readJTable();	
			}
		});
		btnAlterarFilme.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnAlterarFilme.setBounds(204, 431, 151, 21);
		contentPane.add(btnAlterarFilme);
		
		JButton btnExcluirFilme = new JButton("Excluir Passageiro");
		btnExcluirFilme.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(JTPassageiros.getSelectedRow()!= -1) {
					int opcao = JOptionPane.showConfirmDialog(null, "Deseja excluir o Passageiro selecionado?","Exclus�o", JOptionPane.YES_NO_OPTION);
					if(opcao == 0) {
						PassageiroDAO dao = new PassageiroDAO();
						Passageiro p = new Passageiro();
						p.setId_passageiro((int)JTPassageiros.getValueAt(JTPassageiros.getSelectedRow(),0));
						dao.delete(p);
					}
					}else {
						JOptionPane.showMessageDialog(null,"Selecione um passageiro!");
					}
					readJTable();
			}
		});
		btnExcluirFilme.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnExcluirFilme.setBounds(365, 431, 161, 21);
		contentPane.add(btnExcluirFilme);
		
		readJTable();
	}
	
	public void readJTable() {
		DefaultTableModel modelo = (DefaultTableModel) JTPassageiros.getModel();
		modelo.setNumRows(0);
		PassageiroDAO pdao = new PassageiroDAO();
		for(Passageiro p : pdao.read()) {
			modelo.addRow(new Object[] {
				p.getId_passageiro(),
				p.getNome(),
				p.getGenero(),
				p.getRg(),
				p.getCpf(),
				p.getTelefone(),
				p.getEmail(),
				p.getEndereco()
			});
		}
	}
}
