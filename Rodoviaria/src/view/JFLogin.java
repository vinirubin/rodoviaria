package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.GridLayout;
import javax.swing.BoxLayout;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.FlowLayout;
import javax.swing.JSplitPane;
import java.awt.Component;
import javax.swing.Box;
import java.awt.CardLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.SpringLayout;
import net.miginfocom.swing.MigLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

public class JFLogin extends JFrame {

	private JPanel contentPane;
	private JTextField txtUsuario;
	private JPasswordField txtSenha;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFLogin frame = new JFLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFLogin() {
		setTitle("Sistema de Gest\u00E3o de Rodovi\u00E1rias");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 350, 350);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTituloPrincipal = new JLabel("Bem vindo!");
		lblTituloPrincipal.setBounds(10, 30, 316, 30);
		lblTituloPrincipal.setHorizontalAlignment(SwingConstants.CENTER);
		lblTituloPrincipal.setDisplayedMnemonic(' ');
		lblTituloPrincipal.setFont(new Font("Tahoma", Font.BOLD, 25));
		contentPane.add(lblTituloPrincipal);
		
		JLabel lblInformeCredenciais = new JLabel("Informe suas Credencias de Acesso:");
		lblInformeCredenciais.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblInformeCredenciais.setBounds(10, 100, 416, 37);
		contentPane.add(lblInformeCredenciais);
		
		JPanel panelUsuario = new JPanel();
		panelUsuario.setBounds(10, 150, 250, 30);
		contentPane.add(panelUsuario);
		panelUsuario.setLayout(new GridLayout(1, 0, 0, 0));
		
		JLabel lblUsuario = new JLabel("Usu\u00E1rio:");
		lblUsuario.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblUsuario.setHorizontalAlignment(SwingConstants.CENTER);
		panelUsuario.add(lblUsuario);
		
		txtUsuario = new JTextField();
		lblUsuario.setLabelFor(txtUsuario);
		panelUsuario.add(txtUsuario);
		txtUsuario.setColumns(10);
		
		JPanel panelPassword = new JPanel();
		panelPassword.setBounds(10, 190, 250, 30);
		contentPane.add(panelPassword);
		panelPassword.setLayout(new GridLayout(1, 0, 0, 0));
		
		JLabel lblSenha = new JLabel("Senha:");
		lblSenha.setHorizontalAlignment(SwingConstants.CENTER);
		lblSenha.setFont(new Font("Tahoma", Font.PLAIN, 12));
		panelPassword.add(lblSenha);
		
		txtSenha = new JPasswordField();
		lblSenha.setLabelFor(txtSenha);
		panelPassword.add(txtSenha);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 262, 250, 30);
		contentPane.add(panel);
		panel.setLayout(new GridLayout(1, 0, 30, 0));
		
		JButton btnLogin = new JButton("Login");
		panel.add(btnLogin);
		
		JButton btnCadastrar = new JButton("Cadastrar-se");
		panel.add(btnCadastrar);
		
		JPanel panelEsqueceuSenha = new JPanel();
		panelEsqueceuSenha.setBounds(10, 222, 194, 30);
		contentPane.add(panelEsqueceuSenha);
		panelEsqueceuSenha.setLayout(new GridLayout(1, 0, 10, 0));
		
		JLabel lblEsqueceuSenha = new JLabel("Esqueceu a Senha? ");
		lblEsqueceuSenha.setFont(new Font("Tahoma", Font.PLAIN, 10));
		panelEsqueceuSenha.add(lblEsqueceuSenha);
		
		JLabel lblRecuperarSenha = new JLabel("Recuperar a senha");
		lblRecuperarSenha.setFont(new Font("Tahoma", Font.PLAIN, 10));
		panelEsqueceuSenha.add(lblRecuperarSenha);
		lblRecuperarSenha.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblRecuperarSenha.setForeground(Color.magenta);
			}
		});
		lblRecuperarSenha.setBackground(new Color(240, 240, 240));
		lblRecuperarSenha.setForeground(Color.BLUE);
	}
}
