package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class JFAtualizarPassageiro extends JFrame {

	private JPanel contentPane;
	private JTextField cpoNome;
	private JTextField cpoEmail;
	private JTextField cpoTelefone;
	private JTextField cpoEndereco;
	private JTextField cpoCpf;
	private JTextField cpoRg;
	private static int id;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFAtualizarPassageiro frame = new JFAtualizarPassageiro(id);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param id 
	 */
	@SuppressWarnings("rawtypes")
	public JFAtualizarPassageiro(int id) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 333, 344);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		PassageiroDAO pdao = new PassageiroDAO();
		Passageiro p = pdao.read(id);
		
		JLabel lblid = new JLabel("Id Passageiro:");
		lblid.setBounds(189, 28, 69, 13);
		contentPane.add(lblid);
		
		JLabel lblID = new JLabel("");
		lblID.setBounds(264, 28, 45, 13);
		contentPane.add(lblID);
		
		JLabel lblAlterarPassageiro = new JLabel("Alterar Passageiro:");
		lblAlterarPassageiro.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblAlterarPassageiro.setBounds(10, 23, 138, 21);
		contentPane.add(lblAlterarPassageiro);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNome.setBounds(10, 54, 45, 13);
		contentPane.add(lblNome);
		
		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblEmail.setBounds(10, 79, 45, 13);
		contentPane.add(lblEmail);
		
		JLabel lblTelefone = new JLabel("Telefone:");
		lblTelefone.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblTelefone.setBounds(10, 106, 69, 13);
		contentPane.add(lblTelefone);
		
		JLabel lblEndereco = new JLabel("Endereco:");
		lblEndereco.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblEndereco.setBounds(10, 135, 69, 13);
		contentPane.add(lblEndereco);
		
		JLabel lblCpf = new JLabel("CPF:");
		lblCpf.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblCpf.setBounds(10, 164, 45, 13);
		contentPane.add(lblCpf);
		
		JLabel lblRg = new JLabel("RG:");
		lblRg.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblRg.setBounds(10, 189, 45, 13);
		contentPane.add(lblRg);
		
		JLabel lblGenero = new JLabel("Genero:");
		lblGenero.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblGenero.setBounds(10, 217, 55, 13);
		contentPane.add(lblGenero);
		
		cpoNome = new JTextField();
		cpoNome.setToolTipText("Nome");
		lblNome.setLabelFor(cpoNome);
		cpoNome.setBounds(75, 48, 96, 19);
		contentPane.add(cpoNome);
		cpoNome.setColumns(10);
		
		cpoEmail = new JTextField();
		cpoEmail.setToolTipText("Email\r\n");
		lblEmail.setLabelFor(cpoEmail);
		cpoEmail.setColumns(10);
		cpoEmail.setBounds(75, 73, 199, 19);
		contentPane.add(cpoEmail);
		
		cpoTelefone = new JTextField();
		cpoTelefone.setToolTipText("Telefone\r\n");
		lblTelefone.setLabelFor(cpoTelefone);
		cpoTelefone.setColumns(10);
		cpoTelefone.setBounds(75, 100, 96, 19);
		contentPane.add(cpoTelefone);
		
		cpoEndereco = new JTextField();
		cpoEndereco.setToolTipText("Endere\u00E7o");
		lblEndereco.setLabelFor(cpoEndereco);
		cpoEndereco.setColumns(10);
		cpoEndereco.setBounds(75, 129, 199, 19);
		contentPane.add(cpoEndereco);
		
		cpoCpf = new JTextField();
		cpoCpf.setToolTipText("CPF(11): 00000000000");
		lblCpf.setLabelFor(cpoCpf);
		cpoCpf.setColumns(10);
		cpoCpf.setBounds(75, 154, 96, 19);
		contentPane.add(cpoCpf);
		
		cpoRg = new JTextField();
		cpoRg.setToolTipText("RG(10) : 0000000000");
		lblRg.setLabelFor(cpoRg);
		cpoRg.setColumns(10);
		cpoRg.setBounds(75, 183, 96, 19);
		contentPane.add(cpoRg);
		
		JComboBox cpoGenero = new JComboBox();
		lblGenero.setLabelFor(cpoGenero);
		cpoGenero.setModel(new DefaultComboBoxModel(new String[] {"Masculino", "Feminino", "Outro"}));
		cpoGenero.setBounds(75, 214, 96, 21);
		contentPane.add(cpoGenero);
		
		lblID.setText(String.valueOf(p.getId_passageiro()));
		cpoNome.setText(p.getNome());
		cpoEmail.setText(p.getEmail());
		cpoTelefone.setText(p.getTelefone());
		cpoEndereco.setText(p.getEndereco());
		cpoCpf.setText(String.valueOf(p.getCpf()));
		cpoRg.setText(String.valueOf(p.getRg()));
		if (p.getGenero() == "Masculino") {
			cpoGenero.setSelectedItem(0);
		} else if (p.getGenero() == "Feminino") {
			cpoGenero.setSelectedIndex(1);
		}else if (p.getGenero() == "Outro") {
			cpoGenero.setSelectedIndex(2);
		}
		
		JButton btnAlterar = new JButton("Alterar");
		btnAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if ( (cpoNome.getText() == "")                     ||
					 (cpoCpf.getText().matches("-?\\d+") == false) ||
					 (cpoCpf.getText().length() != 11)             ||
					 (cpoEmail.getText() == "")                    ||
					 (cpoRg.getText().matches("-?\\d+") == false)  ||
					 (cpoRg.getText().length() != 10)              ||
					 (cpoEndereco.getText() == "")                 ||
					 (cpoTelefone.getText() == "") ) {
					
					JOptionPane.showMessageDialog(null, "For�o encontrados erros nos Dados inseridos! Favor Verificar.");
	
				} else {
				
					Passageiro p = new Passageiro();
					PassageiroDAO dao = new PassageiroDAO();
					
					p.setId_passageiro(Integer.parseInt(lblID.getText()));
					p.setNome(cpoNome.getText());
					p.setCpf(Long.parseLong(cpoCpf.getText()));
					p.setEmail(cpoEmail.getText());
					p.setRg(Long.parseLong(cpoRg.getText()));
					p.setGenero(cpoGenero.getSelectedItem().toString());
					p.setEndereco(cpoEndereco.getText());
					p.setTelefone(cpoTelefone.getText());
					
					dao.update(p);
					dispose();
				}
				
			}
		});
		btnAlterar.setBounds(10, 258, 85, 21);
		contentPane.add(btnAlterar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setBounds(202, 258, 85, 21);
		contentPane.add(btnCancelar);
		
		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cpoNome.setText("");
				cpoCpf.setText("");
				cpoEmail.setText("");
				cpoRg.setText("");
				cpoCpf.setText("");
				cpoGenero.setSelectedIndex(0);
				cpoEndereco.setText("");
				cpoTelefone.setText("");
			}
		});
		btnLimpar.setBounds(107, 258, 85, 21);
		contentPane.add(btnLimpar);
	}
}
